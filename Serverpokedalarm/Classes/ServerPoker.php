<?php

                    /*
                     * To change this license header, choose License Headers in Project Properties.
                     * To change this template file, choose Tools | Templates
                     * and open the template in the editor.
                     */

                    namespace Serverpokedalarm\Classes;

                    /**
                     * Monitors/pokes the server from where it is instantiated. 
                     * 
                     * Monitors and creates an report, about disc usage, cpu usage, memmory usage, mysql activity. 
                     * Eveluates and raises an alarm if certain conditions are met
                     *
                     * @author wouter wouter@van-den-brink.net
                     * @todo everything 
                     */
                    class ServerPoker {
                        //put your code here
                        protected $settingsarray;
                        public $numberofcores;
                        protected $fullcpuload;
                        protected $gawkpresent;
                        public $topinfoloadaverage;
                        public $founderrors;
                        public $foundapplicationpaths;
                        public $foundimppaths;
                        public $pathdiscinfos;
                        public $serverloadinfos;
                        function __construct($settingsarray='') {
                            if($settingsarray==''){
                                $settingsarray = array("applications"=>array(
                                    "webserver"=>array("application"=>'httpd'),
                                    "databaserserver"=>array("application"=>'mysql',"user"=>"root","passwd"=>"")
                                    )
                                );
                            }
                           //determine number of cpu cores
                           $this->settingsarray=$settingsarray;
                           $numcoresstatus = $this->findNumberOfCores();
                           
                           //can we use awk or gawk?
                           $gawkstatus=$this->gawkpresent=$this->testGawk();
                           if($gawkstatus){
                               //get 15 min average load from top ($14 for gawk) 
                               $toploadaveragestatus =$this->findTopInfoLoadaverage();
                               $testserverloadstatus = $this->findServerLoad();
                               //todo: relate to numberofcores and determine alarm
                               //determine  paths important for deamons/applications,to test for disc space, 
                               $imppathsstatus=$this->findImpPaths();
                               $discspaceinfostatus =$this->findDiscSpaceInfos();
                               
                           }else{//for now this is an deal breaker
                               $this->founderrors['error_gawk']='no_gawk';
                           }
                        }
                        
                        /**
                         * 
                         * @return string panic of boredom
                         */
                        function getServerPanic() {
                            $resultarray = array($this->serverloadinfos['alarm'],$this->pathdiscinfos['alarm']);
                            if(in_array('panic', $resultarray)){
                                echo "------------------oauci";
                                return 'panic';
                            }else{
                                echo "------------------booooring";
                                return 'boredom';
                            }
                        }
                        
                        /**
                         * 
                         * @return boolean
                         */
                        protected function findServerLoad() {
                            $numberofcores=$this->numberofcores;
                            $serverload =$this->topinfoloadaverage;
                            $fullcpuload = $this->fullcpuload;
                            if($fullcpuload!=0){
                                $serverpropload['propload'] =round( $serverload/$fullcpuload,2,PHP_ROUND_HALF_UP);
                                if($serverpropload['propload'] >0.95){
                                    $serverpropload['alarm']='panic';
                                }else{
                                    $serverpropload['alarm']='boredom';
                                }
                            }else{
                                $serverpropload['propload']='unkown';
                                $serverpropload['alarm']='error';
                            }
                            $this->serverloadinfos =$serverpropload;
                            return true;
                        }
                        
                        /**
                         * 
                         * @return boolean
                         */
                        protected function findDiscSpaceInfos() {
                            $imppaths = $this->foundimppaths;
                            $pathdiscinfos =array();
                            $netpanic='boredom';
                            foreach($imppaths as $key=>$value){
                                echo $key . " " . $value;
                                $pathdiscinfos[$key]['path']=$value;
                                $pathdiscinfos[$key]['free_space']=disk_free_space($value);
                                $pathdiscinfos[$key]['totalspace']=disk_total_space($value);
                                if($pathdiscinfos[$key]['totalspace']!=0){
                                    $pathdiscinfos[$key]['proportion']= round ($pathdiscinfos[$key]['free_space']/$pathdiscinfos[$key]['totalspace'],2,PHP_ROUND_HALF_UP);
                                    //round ( $val ,2 ,PHP_ROUND_HALF_UP ) 
                                    if($pathdiscinfos[$key]['proportion']<0.1){
                                        $pathdiscinfos[$key]['alarm']='panic';
                                        $netpanic="panic";
                                    }else{
                                        $pathdiscinfos[$key]['alarm']='boredom';
                                    }
                                }
                            }
                            $pathdiscinfos['alarm']=$netpanic;
                            
                            $this->pathdiscinfos =$pathdiscinfos;
                            print_r($pathdiscinfos);
                            return true;
                        }
                        
                        /**
                         * 
                         * @return boolean
                         */
                        protected function findImpPaths() {
                            $applications=$this->settingsarray['applications'];
                           // print_r($applications);
                            $foundpaths =array();
                            $foundpaths[]=realpath( $_SERVER['DOCUMENT_ROOT']);
                            $foundpaths[]=realpath( __DIR__);
                            foreach($applications as $key=>$value){
                                
                                echo $key . " " .$value['application'];
                                switch ($key . " " .$value['application']){
                                    case 'webserver httpd':
                                        echo "HHHHHJJJJJJJJJJJJJJJJJJ";
                                        /* apparently exec does not know as much as bash; apache2 gives apache2 unkown */;
                                        $foundpaths[]=realpath($this->findImpPathsforHttpd('/usr/sbin/httpd'));
                                        break;
                                    case 'webserver apache2':
                                        /* apparently exec does not know as much as bash; apache2 gives apache2 unkown */;
                                        $foundpaths[]=realpath($this->findImpPathsforHttpd('/usr/sbin/apache2'));
                                        break;
                                    case 'databaserserver mysql':
                                        $foundpaths[]=realpath($this->findImpPathsFormysql());
                                        break;
                                    default :
                                        $this->founderrors['applications']='no_applications';
                                
                                }
                            }
                            $this->foundapplicationpaths=$foundpaths;
                            $this->foundimppaths=$foundpaths;
                            return true;
                        }

                        protected function findImpPathsforHttpd($webservername) {
                            //echo "httpdpathsss";
                            /* gawk, a unix/linux interpreted text utility language and interpreter, 
                            *  is a awk version, see for both http://www.grymoire.com/Unix/Awk.html 
                            *  exec() performs a shell command and returns an array where every output 
                            *  line from the executed command is an array item
                            *  where 0 is the first. httpd is running apache on centos:: i guess this 
                            *  will differ on for instance ubuntu
*/
                            echo "hierdan";
                             //$command ='/bin/bash -c "apache2 -V 2>&1"';
                             //exec($command,$results);
                             //print_r($results);
                             $command =$webservername . ' -V | gawk \'/HTTPD_ROOT/ {gsub(/((HTTPD_ROOT=)|")/,"",$2);print $2}\'';
                             //$command = 'ls ---  2>&1';
                            
                            echo $command;
                             $respons = exec($command,$outputarray);
                             print_r($outputarray);
                             if($respons){
                                 echo "0000000000000000000000000000";
                                 print_r($outputarray);
                                 return realpath($outputarray[0]);
                             }else{
                                 return '';
                             }
                        }
                        
                        protected function findImpPathsFormysql() {
                            //print_r($this->settingsarray['applications']['databaserserver']);
                            $database=$this->settingsarray['applications']['databaserserver'];
                             $command='mysql -s -N -u'. $database['user'] .' -p'. $database['passwd'] .' information_schema -e \'SELECT Variable_Value FROM GLOBAL_VARIABLES WHERE Variable_Name = "datadir"\' ;';// 2>$1 puts standard error in standard output: else $outputarray is empty when an error occurs
                             $respons =exec($command,$outputarray);
                             if($respons){

                                  return realpath($outputarray[0]);
                             }else{ 
                                 
                                 return '';
                             }
                             //print_r($returndus);
                             //print_r($outputarray);
                            
                        }

                        /**
                         * Get an report as an array.
                         * 
                         * @return array
                         */
                        public function getReportArray(){
                            $returnarray = array();
                            $returnarray['errors'] =$this->founderrors;
                            $returnarray['cores']=$this->numberofcores;
                            $returnarray['gawk'] = $this->gawkpresent;
                            $returnarray['load_average']=$this->topinfoloadaverage;
                            //$returnarray['settingsarray']=$this->settingsarray;
                            $returnarray['foundapplicationpaths']=$this->foundapplicationpaths;
                            $returnarray['foundimppaths']=$this->foundimppaths;
                            $returnarray['$pathdiscinfos']=$this->pathdiscinfos;
                            $returnarray['$serverloadinfos'] = $this->serverloadinfos;
                            
                            return $returnarray;
                        }

                        /**
                         * Determine number of cpu cores. 
                         * 
                         * Using command nproc determines number of cpu cores
                         * and set properties numberofcores and fulcpuload
                         * 
                         * @see http://www.cyberciti.biz/faq/linux-get-number-of-cpus-core-command/
                         * 
                         * @return boolean true if ok, else false
                         */
                        protected function findNumberOfCores() {
                            if($this->command_exist('nproc')){
                                $command = 'nproc ';
                                exec($command,$outputarray);
                                $this->numberofcores = (int) $outputarray[0];
                                $this->fullcpuload = (float) $outputarray[0];
                                return true;
                            }else{
                                return false;
                            }
                        }
                        
                        /**
                         * Tests if gawk of awk command is present on server.
                         * 
                         * @return boolean|string
                         */
                        protected function testGawk(){
                            //test for gawk
                            $gawk='';
                            if($this->command_exist('gawk')){
                                $gawk='gawk';
                            }elseif($this->command_exist('awk')){
                                $gawk = 'awk';
                            }else{
                                ;$gawk=false;
                            }
                            return $gawk;
                        }
                        
                        /**
                         * Utility function: Test presence of command on this server.
                         * 
                         * @see http://stackoverflow.com/questions/12424787/how-to-check-if-a-shell-command-exists-from-php
                         * @param type $cmd
                         * @return boolean
                         */
                        protected function command_exist($cmd) {
                            $returnVal = shell_exec("which $cmd");
                            return (empty($returnVal) ? false : true);
                        }
                        
                        /**
                         * Find 15 minutes load average from top command output.
                         * (g)awk is used for now together with top (
                         * @return boolean
                         */
                        protected function findTopInfoLoadaverage(){
                            $gawk =$this->gawkpresent;
                            if($gawk&&$this->command_exist('top')){
                               // echo "gawk: $gawk top: " . $this->command_exist('top') . 'dus';
                                $command='top -b -n1 | head -1 | '.$gawk.' \'/load average:/ {gsub(/\,/,"",$14);print $14watch }\'';
                                //$command='top -b -1 | head -6 ';
                                exec($command,$outputarray);
                                //print_r($outputarray);
                                $this->topinfoloadaverage =$outputarray[0];
                                return true;
                            }else{
                                $this->topinfoloadaverage ='error_no_gawk_or_top';//todo: implement an no (g)awk php solution
                                //$command='top -b -n1 | head -1 ' and then find with preg_match ofzo
                                return false;
                            }
                        }

                        /**
                         * 
                         * @return int
                         */
                        public function getNumberofcores(){
                            return $this->numberofcores;
                        }
                        /**
                         * 
                         * @return float
                         */
                        public function getTopinfoLoadaverage(){
                            return $this->topinfoloadaverage;
                        }
                    }//end of class
